# Script pour automatiser la sauvegarde et la restauration des extensions VS code
# version 1.0
# Auteur : Antoine RICHARD

# Force le type d'execution

Set-ExecutionPolicy Unrestricted

function Show-Menu
{
     param (
           [string]$Title = 'Save/Restore VS code extensions'
     )
     cls
     Write-Host "================ $Title ================"
    
     Write-Host "1: Save extensions"
     Write-Host "2: Restore extensions (without network connection"
     Write-Host "3: Restore extensions (need network connection)"
     Write-Host "Q: Quit."
}


do
{
     Show-Menu
     $input = Read-Host "Please make a selection"
     switch ($input)
     {
           '1' {
                cls
                '================ Save extensions ================'
                code --list-extensions > extensions.list
                'Extensions have been saved'
           } '2' {
                cls
                '================ Restore extensions ================'
                cd extensions
                Get-ChildItem . -Filter *.vsix | ForEach-Object { code --install-extension $_.FullName }
                'Extensions have been restored'
            }'3' {
                cls
                '================ Restore extensions ================'
                cat extensions.list |% { code --install-extension $_}
                'Extensions have been saved'
           } 'q' {
                return
           }
     }
     pause
}
until ($input -eq 'q')