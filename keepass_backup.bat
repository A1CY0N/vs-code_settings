: Time vars
Set jour=%date:~0,2%
Set mois=%date:~3,2%
Set annee=%date:~6,4%
Set heure=%time:~0,2%
Set minute=%time:~3,2%

REM Save Keepass DB
    : Original Keepass DB
    Set Keepass_DB_Name="toto.kdbx"
    Set Keepass_DB_Path="C:\Users\toto"
    Set Initial_DB_File="%Keepass_DB_Path%\%Keepass_DB_Name%"

    : Save repertory 
    Set Sauv_DB_Path="P:\Sauvegardes\Keepass"
    Set Sauv_DB_File="%Sauv_DB_Path%\[%jour%-%mois%-%annee%-%heure%h%minute%]%Keepass_DB_Name%"

REM Save Keepass Key
    : Original Keepass DB
    Set Keepass_Key_Name="toto.key"
    Set Keepass_Key_Rep_Path="C:\Users\toto"
    Set Initial_Key_File="%Keepass_Key_Rep_Path%\%Keepass_Key_Name%"

    : Save repertory 
    Set Sauv_Key_Path="P:\Sauvegardes\Keepass\keys"
    Set Sauv_Key_File="%Sauv_Key_Path%\[%jour%-%mois%-%annee%-%heure%h%minute%]%Keepass_Key_Name%"

: Copy with txt history
xcopy %Initial_DB_File%  %Sauv_DB_File%* /R /Y >> %Sauv_DB_Path%\Sauv_Keepass_DB.txt

: Key copy only if it has been modified
if EXIST "%Sauv_Key_Path%\*.key" (
    FOR %%a IN ("%Keepass_Key_Rep_Path%\*.key") DO (
        FOR %%b IN ("%Sauv_Key_Path%\*.key") DO (
            : Size & modif date vars
            Set Initial_Key_File_Size=%%~za
            Set Sauv_Key_File_Size=%%~zb
            Set Initial_Key_File_Modif=%%~ta
            Set Sauv_Key_File_Modif=%%~tb

            if "%Initial_Key_File_Size%" == "%Sauv_Key_File_Size%" (
                : If initial and sauv keys have same size
                if "%Initial_Key_File_Modif%" == "%Sauv_Key_File_Modif%" (
                    : If initial key have the same last modif date as the sauv one
                    : The sauv key = original key
                ) else (
                    : The 2 keys haven't same date modif
                    xcopy "%Initial_Key_File%"  %Sauv_Key_File%* /R /Y >> %Sauv_Key_Path%\Sauv_Keepass_keys.txt
                )
                
            ) else (
                 : The 2 keys haven't same size
                xcopy "%Initial_Key_File%"  %Sauv_Key_File%* /R /Y >> %Sauv_Key_Path%\Sauv_Keepass_keys.txt
            )
            
        )
    )
) else (
    : The sauv key doesn't exist 
    xcopy "%Initial_Key_File%"  %Sauv_Key_File%* /R /Y >> %Sauv_Key_Path%\Sauv_Keepass_keys.txt
)